function setup() {
  width = 1000
  height = 1000
  rectSize = 50
  noStroke()

  createCanvas(width,height);
  colorMode(HSB, 100)
  moveY = round(random(1,7))
  moveX = round(random(1,7))
  print("moveX: " + moveX + ", moveY: " + moveY)

//  moveY = 5
//  moveX = 5
  rectHue = 0
  y = 0
  x = 0
  won = false
}

function draw() {
  fill(rectHue,50,85);

  background(85);
  rect(x,y,rectSize,rectSize)
  y = y + moveY
  x = x + moveX
  rectHue = rectHue + 1
  if ((y + rectSize > height) || (y < 0)) {
    moveY = moveY * -1
  }
  if ((x + rectSize > width) || (x < 0)) {
    moveX = moveX * -1
  }
  if (rectHue > 100) {
    rectHue = rectHue - 100
  }
  if (detectCorner()) {
    won = true
  }
  if (key === 'x') {
    won = true
  }
  
  if (won == true) {
    comgerts()
  }

}

function detectCorner() {
  if ((x == 0) && (y == 0)) {
    return true
  }
  else if ((x == 0) && (y == height - rectSize)) {
    return true
  }
  else if ((x == width - rectSize) && (y == 0)) {
    return true
  }
  else if ((x == width - rectSize) && (y == height - rectSize)) {
    return true
  }
}

function comgerts() {
  rectSize = rectSize + 20
  x = x - 10
  y = y - 10
  moveX = 0
  moveY = 0
  if (rectSize + y > width) {
    textAlign(CENTER,TOP)
    textSize(70)
    fill(rectHue/2,100,100)
    text('comgerts\nu did it\n yey', width/2, height/4)
  }
}

//function keyTyped() {
//  if (key === 'x') {
//    return true
//  }
//}

// this doesn't work as intended at all lol
function keyPressed() {
  if (keyCode === UP_ARROW) {
    moveY = moveY + 2
    moveX = moveX + 2
  } else if (keyCode === DOWN_ARROW) {
    moveY = moveY - 2
    moveX = moveX - 2
  }
}