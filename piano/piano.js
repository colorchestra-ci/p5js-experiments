let synth
let bonks = []
let backgroundColor = 0
let borderColor = 50
let borderSpeed = 0.2
let borderWidth = 50
let bonkWidth = 20
let bonkHeight = 80

class Bonk {
  constructor(color, posY, speed, note) {
    this.posX = borderWidth
    this.posY = posY
    this.speed = speed
    this.color = color
    this.note = note
    this.collisionLock = false
    this.showWhiteBorder = false
  }

  move() {
    this.posX = this.posX + this.speed
  }

  checkCollision() {
    this.showWhiteBorder = false

    if ((this.posX <= borderWidth && this.collisionLock == false) || (this.posX + bonkWidth >= width - borderWidth && this.collisionLock == false)){
      this.collisionLock = true
      this.playNote()
      this.showWhiteBorder = true
      this.speed = -this.speed
    }

    // remove collisionLock
    // going right                                   going left
    if ((this.speed > 0 && this.posX > width / 2) || (this.speed < 0 && this.posX < width / 2)) {
      this.collisionLock = false
    }
  }

  display() {
    fill(this.color)
    rect(this.posX, this.posY, bonkWidth, bonkHeight)
    noFill()
    
    if (this.showWhiteBorder) {
      stroke(255,255,255)
      strokeWeight(3)
      rect(this.posX, this.posY, bonkWidth, bonkHeight)
      noStroke()
    }
  }

  playNote() {
    let velocity = 0.8;
    // time from now (in seconds)
    let time = 0;
    // note duration (in seconds)
    let dur = 1/6;

    synth.play(this.note, velocity, time, dur);

  }
}

function setup() {
  width = 800
  height = 800
  noStroke()

  synth = new p5.PolySynth();
  userStartAudio();
  synth.setADSR(0.1, 0.2, 0.2, 0)

  createCanvas(width,height);

  bonks[0] = new Bonk(color(255, 0, 0), 10, 3, "C5")
  bonks[1] = new Bonk(color(255, 130, 0), 110, 3.1, "B5")
  bonks[2] = new Bonk(color(255, 255, 0), 210, 3.2, "A5")
  bonks[3] = new Bonk(color(0, 255, 0), 310, 3.3, "G4")
  bonks[4] = new Bonk(color(0, 255, 255), 410, 3.4, "F4")
  bonks[5] = new Bonk(color(0, 0, 255), 510, 3.5, "E4")
  bonks[6] = new Bonk(color(170, 0, 255), 610, 3.6, "D4")
  bonks[7] = new Bonk(color(255, 0, 161), 710, 3.7, "C4")
}

function draw() {
  background(backgroundColor);

  // grey border
  fill(borderColor)
  // left
  rect(0, 0, borderWidth, height)
  // right
  rect(width - borderWidth, 0, borderWidth, height)
  
  for (bonk of bonks) {
    bonk.move()
    bonk.display()
    bonk.checkCollision()
  }
  borderWidth += borderSpeed

  // stop if end is reached
  if (borderWidth > width / 2) {
    background(borderColor)
    console.log("Stopping execution")
    noLoop()
    for (bonk of bonks) {
      bonk.posX = width / 2 - bonkWidth / 2
      bonk.showWhiteBorder = false
      bonk.display()
      bonk.playNote()
    }
  }
}
