function setup() {
  width = 800
  height = 800
  rectSize = 50
  noStroke()

  createCanvas(width,height);
  colorMode(HSB, 100)
  moveY = round(random(1,7))
  moveX = round(random(1,7))
  print("moveX: " + moveX + ", moveY: " + moveY)

//  moveY = 5
//  moveX = 5
  rectHue = 0
  y = 100
  x = 100
}

let borderWidth = 50
function draw() {
  background(0);

  // grey border
  fill(50)
  // left
  rect(0, 0, borderWidth, height)
  // right
  rect(width - borderWidth, 0, borderWidth, height)
  
  // bunt
  fill(rectHue,50,85);
  rect(x,y,rectSize,rectSize)
  y = y + moveY
  x = x + moveX
  rectHue = rectHue + 1
  if ((y + rectSize > height) || (y < 0)) {
    moveY = moveY * -1
  }
  if ((x + rectSize > width - borderWidth) || (x < 0 + borderWidth)) {
    moveX = moveX * -1
  }
  if (rectHue > 100) {
    rectHue = rectHue - 100
  }
  borderWidth = borderWidth + 0.5

}
