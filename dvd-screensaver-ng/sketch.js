let squaresNumber;
let sq;
let speeds;
let img;

function setup() {
  width = 800
  height = 600
  rectSize = 72
  rectW = 128
  rectH = 60
  let squaresNumber =  1

  noStroke()
  createCanvas(width,height);
  colorMode(HSB, 100)
  img = loadImage('dvd2.png')

  let speeds = [-7, -6, -5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 6, 7]
  sq = []
  for (let i = 0; i < squaresNumber ; i++) {
    sq[i] = new Square()
    print(sq[i])
  }
  rectHue = 0
  y = 0
  x = 0
  won = false
}

function draw() {
  print(frameRate())
  background(55);

  detectCollision()
  for (let i = 0; i < sq.length; i++) {
    sq[i].detectWall()
    sq[i].display()
    sq[i].move()
  }

  if (detectCorner()) {
    won = true
  }
  if (key === 'x') {
    won = true
  }
  
  /*
  if (won == true) {
    comgerts()
  }
  */

}

class Square {
  constructor() {
    this.moveY = round(random(-7,7))
    this.moveX = round(random(-7,7))
    this.hue = round(random(100))
    this.x = round(random(width-rectW))
    this.y = round(random(height-rectH))
  }
  move() {
    this.x += this.moveX
    this.y += this.moveY
  }
  display() {
    tint(this.hue, 80, 80, 80)
    image(img,this.x, this.y, rectW, rectH)
//    fill(this.hue,50,85)
//    rect(this.x, this.y, rectSize, rectSize)
    if (this.hue <= 100) {
      this.hue += 1
    } else {
      this.hue -= 100
    }
  }
  detectWall() {
    if ((this.y + rectH > height) || (this.y < 0)) {
      this.moveY = this.moveY * -1
    }
    if ((this.x + rectW> width) || (this.x < 0)) {
      this.moveX = this.moveX * -1
    }
    if (rectHue > 100) {
      rectHue = rectHue - 100
    }
  }
}

function detectCollision() {
  for (let i in sq) {
    for (let j in sq) {
      // i ist weiter RECHTS als j                                || i ist weiter UNTEN als j
      if ((sq[i].x >= sq[j].x) && (sq[i].x <= sq[j].x + rectSize) && (sq[i].y >= sq[j].y) && (sq[i].y <= sq[j].y + rectSize)) {
        sq[i].moveX = sq[i].moveX * -1
        sq[j].moveX = sq[j].moveX * -1
      }

      // i ist weiter UNTEN als j                                 || i ist weiter RECHTS als j
      if ((sq[i].y >= sq[j].y) && (sq[i].y <= sq[j].y + rectSize) && (sq[i].x >= sq[j].x) && (sq[i].x <= sq[j].x + rectSize)) {
        sq[i].moveY = sq[i].moveY * -1
        sq[j].moveY = sq[j].moveY * -1
      }

      // i ist weiter RECHTS als j                                || i ist weiter OBEN als j
      if ((sq[i].x >= sq[j].x) && (sq[i].x <= sq[j].x + rectSize) && (sq[j].y >= sq[i].y) && (sq[j].y <= sq[i].y + rectSize)) {
        sq[i].moveX = sq[i].moveX * -1
        sq[j].moveX = sq[j].moveX * -1
      }

      // i ist weiter UNTEN als j                                 || i ist weiter LINKS als j
      if ((sq[i].y >= sq[j].y) && (sq[i].y <= sq[j].y + rectSize) && (sq[j].x >= sq[i].x) && (sq[j].x <= sq[i].x + rectSize)) {
        sq[i].moveY = sq[i].moveY * -1
        sq[j].moveY = sq[j].moveY * -1
      }
    }
  }
}

function detectCorner() {
  if ((x == 0) && (y == 0)) {
    return true
  }
  else if ((x == 0) && (y == height - rectSize)) {
    return true
  }
  else if ((x == width - rectSize) && (y == 0)) {
    return true
  }
  else if ((x == width - rectSize) && (y == height - rectSize)) {
    return true
  }
}

function comgerts() {
  rectSize = rectSize + 20
  x = x - 10
  y = y - 10
  moveX = 0
  moveY = 0
  if (rectSize + y > width) {
    textAlign(CENTER,TOP)
    textSize(70)
    fill(rectHue/2,100,100)
    text('comgerts\nu did it\n yey', width/2, height/4)
  }
}

//function keyTyped() {
//  if (key === 'x') {
//    return true
//  }
//}

// this doesn't work as intended at all lol
function keyPressed() {
  if (keyCode === UP_ARROW) {
    moveY = moveY + 2
    moveX = moveX + 2
  } else if (keyCode === DOWN_ARROW) {
    moveY = moveY - 2
    moveX = moveX - 2
  }
}
